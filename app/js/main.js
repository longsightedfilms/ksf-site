function formatMoney(n, c, d, t) {
    var c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? " " : d,
        t = t == undefined ? " " : t,
        s = n < 0 ? "-" : "",
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
        j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

function Parallax(options) {
    options = options || {};
    this.nameSpaces = {
        wrapper: options.wrapper || '.parallax',
        layers: options.layers || '.parallax-layer',
        deep: options.deep || 'data-parallax-deep'
    };
    this.init = function () {
        var self = this,
            parallaxWrappers = document.querySelectorAll(this.nameSpaces.wrapper);
        for (var i = 0; i < parallaxWrappers.length; i++) {
            (function (i) {
                window.addEventListener('mousemove', function (e) {
                    var x = e.clientX,
                        y = e.clientY,
                        layers = parallaxWrappers[i].querySelectorAll(self.nameSpaces.layers);
                    for (var j = 0; j < layers.length; j++) {
                        (function (j) {
                            var deep = layers[j].getAttribute(self.nameSpaces.deep),
                                disallow = layers[j].getAttribute('data-parallax-disallow'),
                                itemX = (disallow && disallow === 'x') ? 0 : x / deep,
                                itemY = (disallow && disallow === 'y') ? 0 : y / deep;
                            if (disallow && disallow === 'both') return;
                            layers[j].style.transform = 'translateX(' + itemX + '%) translateY(' + itemY + '%)';
                        })(j);
                    }
                })
            })(i);
        }
    };
    this.init();
    return this;
}

var target = window.location.hash,
    target = target.replace('#', '');

window.location.hash = "";

$(window).on('load', function () {
    if (target) {
        $('html, body').animate({
            scrollTop: $('#' + target).offset().top + 350
        }, 1000);
    }
});

$(document).ready(function(){
    ajaxInit();
    /*if (window.matchMedia("(min-width:992px)").matches) {
        var offsetFromTop = window.matchMedia("(min-width:1441px)").matches ? 156 : 72;
        var viewportTop = $(window).scrollTop();
        if (viewportTop > offsetFromTop) {
            $("body").addClass("header__sticky");
        } else {
            $("body").removeClass("header__sticky");
        }

        $(window).scroll(function () {
            var viewportTop = $(window).scrollTop();
            if (viewportTop > offsetFromTop) {
                $("body").addClass("header__sticky");
            } else {
                $("body").removeClass("header__sticky");
            }
        })
    }*/
})

var mfpOpen = true;

function ajaxInit() {
    new Parallax();
    initTimer();
    inlineSVG.init({
        svgSelector: 'img.svg-inline', // the class attached to all images that should be inlined
        initClass: ''
    });

    $('.founder .button').click(function() {
        $('.founder').addClass('clicked');
        $(this).remove();
    })

    // Blacklist Nav Button
    $('.blacklist-members__nav li a').on('click', function(event) {
        event.preventDefault();
        var filter = $(this).attr('data-filter');
        
        $('.blacklist-members__nav li a').removeClass('active');
        $(this).addClass('active');

        $('.blacklist-members__list li').removeClass('hidden');

        if (filter !== 'all') {
            var regex = new RegExp('\\b\\w*' + filter + '\\w*\\b');
            $('.blacklist-members__list li').addClass('hidden').filter(function () {
                return regex.test($(this).attr('data-filter'));
            }).removeClass('hidden');
        }
    })

    // Members Nav Button
    $('.members__nav li a').on('click', function(event) {
        event.preventDefault();
        var filter = $(this).attr('data-filter');
        
        $('.members__nav li a').removeClass('active');
        $(this).addClass('active');

        $('.members__list li').removeClass('hidden');

        if (filter !== 'all') {
            var regex = new RegExp('\\b\\w*' + filter + '\\w*\\b');
            $('.members__list li').addClass('hidden').filter(function () {
                return regex.test($(this).attr('data-filter'));
            }).removeClass('hidden');
        }
    })

    // Portfolio Nav Button
    $('.portfolio .portfolio__nav li a').on('click', function(event) {
        event.preventDefault();
        var filter = $(this).attr('data-filter');
        
        $('.portfolio .portfolio__nav li a').removeClass('active');
        $(this).addClass('active');

        $('.portfolio .portfolio__list li').removeClass('hidden');

        if (filter !== 'all') {
            var regex = new RegExp('\\b\\w*' + filter + '\\w*\\b');
            $('.portfolio .portfolio__list li').addClass('hidden').filter(function () {
                return regex.test($(this).attr('data-filter'));
            }).removeClass('hidden');
        }
    })

    /*$('.header .nav-item .nav-link').on('mouseenter', function() {
        if (window.location.pathname != ('/' + $(this).attr('href'))) {
            // Change page
            history.pushState(null, null, $(this).attr('href'));
            $('.wrapper').load($(this).attr('href') + ' .wrapper', null, ajaxInit);
            // Add class to header
            $('.header').attr('class', 'header');
            $('.header').addClass($(this).attr('data-class'));
            // Add class to active nav item
            $('.header .nav-item').attr('class', 'nav-item');
            $(this).parent().addClass('active');
        }
    })*/

    // Last orders table button
    $('.recomendations__button-table').on('click', function() {
        $('.recomendations__table li.hidden').removeClass('hidden');
        $('.recomendations__table').addClass('show');
        $(this).remove();
    })

    $('.examples .button').on('click', function() {
        $('.examples__container li.hidden').removeClass('hidden');
        $(this).remove();
    })

    var profilePortfolioSlider = new Swiper('.profile__slider-portfolio', {
        grabCursor: true,
        loop: true,
        slidesPerView: 1,

        // Navigation arrows
        navigation: {
            nextEl: '.profile-portfolio .profile__slider-portfolio-nav .swiper-button-next',
            prevEl: '.profile-portfolio .profile__slider-portfolio-nav .swiper-button-prev',
        }
    })

    var profileCertsSlider = new Swiper('.profile-certificates .profile__slider', {
        grabCursor: true,
        loop: true,
        slidesPerView: 3,
        slidesPerGroup: 3,
        spaceBetween: 30,

        pagination: {
            el: '.profile-certificates .profile__slider .swiper-pagination',
        },
        // Navigation arrows
        navigation: {
            nextEl: '.profile-certificates .profile__slider-nav .swiper-button-next',
            prevEl: '.profile-certificates .profile__slider-nav .swiper-button-prev',
        }
    })
    
    var profileAchievmentsSlider = new Swiper('.profile-achievments .profile__slider', {
        grabCursor: true,
        loop: true,
        slidesPerView: 3,
        slidesPerGroup: 3,
        spaceBetween: 30,

        pagination: {
            el: '.profile-achievments .profile__slider .swiper-pagination',
        },
        // Navigation arrows
        navigation: {
            nextEl: '.profile-achievments .profile__slider-nav .swiper-button-next',
            prevEl: '.profile-achievments .profile__slider-nav .swiper-button-prev',
        }
    })

    var reviewsSlider = new Swiper('.profile__slider-reviews', {
        effect: 'coverflow',
        grabCursor: true,
        loop: true,
        slidesPerView: 'auto',
        loopedSlides: 4,
        centeredSlides: true,
        autoHeight: true,
        coverflowEffect: {
            rotate: 0,
            slideShadows: false,
            stretch: 505,
            depth: 100
        },

        // Navigation arrows
        navigation: {
            nextEl: '.profile__slider-reviews .swiper-button-next',
            prevEl: '.profile__slider-reviews .swiper-button-prev',
        }
    })


    $('.profile-certificates .profile__slider .swiper-slide a').magnificPopup({
        type: 'image',
        fixedContentPos: false,
        gallery: {
            enabled: true
        },
        callbacks: {
            open: function () {
                $('body').addClass('noscroll');

            },
            close: function () {
                $('body').removeClass('noscroll');
            }
        }
    });

    $('.profile-achievments .profile__slider .swiper-slide a').magnificPopup({
        type: 'image',
        fixedContentPos: false,
        gallery: {
            enabled: true
        },
        callbacks: {
            open: function () {
                $('body').addClass('noscroll');

            },
            close: function () {
                $('body').removeClass('noscroll');
            }
        }
    });

    $('.examples .example a').click(function(event) {
        event.preventDefault();
        var type = $(this).parent().hasClass('example_red') ? 'iframe' : 'image';
        $.magnificPopup.open({
            items: { src: $(this).attr('href') },
            type: type,
            fixedContentPos: false,
            callbacks: {
                open: function () {
                    $('body').addClass('noscroll');
                    
                },
                close: function () {
                    $('body').removeClass('noscroll');
                }
            }
        });
    });

    $('.profile-portfolio__fav .info .button, .swiper-slide .grid .cell .info .button').on('click', function(event){
        event.preventDefault();
        gridGalleryInit(event);
    });

    $('.profile__slider-portfolio-nav .button').on('click', function() {
        $(this).parent().remove();
        profilePortfolioSlider.destroy(true, true);
        $('.profile__slider-portfolio .swiper-wrapper .swiper-slide-ingrid').clone().appendTo('.profile-portfolio__grid');
        $('.profile__slider-portfolio .swiper-wrapper .swiper-slide-ingrid').remove();
        $('.profile-portfolio__grid').slideDown(800);
        $('.profile-portfolio__grid .cell .info .button').on('click', function(event) {
            event.preventDefault();
            gridGalleryInit(event);
        });
    })

    $('a.scrollto').click(function(event) {
        event.preventDefault();
        var target = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(target).offset().top + 350
        }, 1000);
    })

    $('a.pdf-link').magnificPopup({
        type: 'iframe',
        fixedContentPos: false,
        callbacks: {
            open: function () {
                $('body').addClass('noscroll');

            },
            close: function () {
                $('body').removeClass('noscroll');
            }
        }
    });
    
}

$(window).scroll(rotateHIWCircle);
$(window).scroll(movePill);

function rotateHIWCircle() {
    $('.how-it-works img').css({ "transform": "rotate(" + $(this).scrollTop() * 2 + "deg)" })
}

function movePill() {
    try {
        var pillOffset = $('.would-hard__images').offset().top - 600;
        var pillOffsetStop = pillOffset + $('.would-hard__images').height() + 144;

        if ($(this).scrollTop() >= pillOffset && $(this).scrollTop() <= pillOffsetStop) {
            var transZ = 100 - ($(this).scrollTop() - pillOffset) * 0.09;
            if ($(this).scrollTop() - pillOffset > 550 && $(this).scrollTop() - pillOffset < 760) {
                transZ = 100 - ($(this).scrollTop() - pillOffset) * 0.05;
            }
            $('.would-hard__images .pill').css({
                "transform": "translateY(" + ($(this).scrollTop() - pillOffset) + "px)"
                + "translateZ(" + transZ + "px)"
            })
        }
    } catch (error) {}
}

function openModal(modal, event) {
    try {
        event.preventDefault();
        event.stopPropagation();
    } catch (e) {
        
    }
    $.magnificPopup.open({
        items: { src: modal},
        type: 'inline',
        removalDelay: 300,
        mainClass: 'mfp-fade',
        fixedContentPos: false,
        callbacks: {
            open: function () {
                $('body').addClass('noscroll');

            },
            close: function () {
                $('body').removeClass('noscroll');
            }
        }
    }, 0);
}


function throttle(fn, wait) {
    var time = Date.now();
    return function () {
        if ((time + wait - Date.now()) < 0) {
            fn();
            time = Date.now();
        }
    }
}

function cutSeconds(seconds) {
    var seconds = String(seconds);
    if (seconds == "-1") {
        return '0';
    } else {
        return seconds.length == 2 ? seconds.substr(1, 1) : seconds;
    }
}


function initTimer() {
    try {
        var date = $('.membership__container .center .timer').attr('data-countdown').split('.').map(function (currentValue) {
            return Number(currentValue);
        });

        var day = date[0];
        var month = date[1] - 1 >= 0 ? date[1] - 1 : date[1];
        var year = date[2]

        //Countdown timer
        var timer = countdown(new Date(year, month, day), function (countdown) {
            $('.pay-block .timer .timer-block.hours p').html(countdown.hours);
            $('.pay-block .timer .timer-block.minutes p').html(countdown.minutes);

            $('.pay-block .timer .timer-block.seconds p.current').html(countdown.seconds);
            $('.pay-block .timer .timer-block.seconds p.prev').html(cutSeconds(countdown.seconds + 1));
            $('.pay-block .timer .timer-block.seconds p.next').html(cutSeconds(countdown.seconds - 1));
        },
            countdown.HOURS | countdown.MINUTES | countdown.SECONDS);
    } catch (error) { }
}

function gridGalleryInit(event) {
    try {
        var images = $(event.target).parent().find('.gallery a');
        var items = [];

        images.each(function (index, element) {
            items.push({
                src: $(element).attr('href'),
                type: $(element).attr('href').includes('.pdf') ? 'iframe' : 'image'
            })
        })

        $.magnificPopup.open({
            items: items,
            type: 'image',
            gallery: {
                enabled: true
            },
            fixedContentPos: false,
            callbacks: {
                open: function () {
                    $('body').addClass('noscroll');
                },
                close: function () {
                    $('body').removeClass('noscroll');
                }
            }
        });
    } catch (error) {
        console.log(error);
    }
}